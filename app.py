import datetime, json, csv
import pandas as pd
from csv import reader

from flask import Flask, request, jsonify, send_file
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import IntegrityError
from marshmallow import Schema, fields, ValidationError, pre_load
from flask_marshmallow import Marshmallow
from sqlalchemy import update
from flask_cors import CORS
from flask_csv import send_csv

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///papad.db"
db= SQLAlchemy(app)
CORS(app)

# MODELS
class Station(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    station_name = db.Column(db.String(100), unique=True)
    station_image = db.Column(db.String(300))

class Record(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    audio_url = db.Column(db.String(300), nullable=False)
    tags = db.Column(db.String(100))
    img_tags = db.Column(db.String(500))
    upload_date = db.Column(db.String(80))
    station_id = db.Column(db.Integer, db.ForeignKey("station.id"), nullable=False)
    station = db.relationship("Station", backref=db.backref("records", lazy="dynamic"))


#Schema
class StationSchema(Schema):
    
    id = fields.Int(dump_only=True)
    station_name = fields.Str()
    station_image = fields.Str()
    #formatted_station = fields.Method("format_station",dump_only=True)

    def format_station(self, station):
        return "{},{}".format(station.station_name, station.station_image)

#validator
def must_not_be_blank(data):
    if not data:
        raise ValidationError("Data not provided.")


class RecordSchema(Schema):
    id = fields.Int(dump_only=True)
    station = fields.Nested(StationSchema, validate=must_not_be_blank)
    audio_url = fields.Str(required=True, validate=must_not_be_blank)
    tags = fields.Str(required=True, validate=must_not_be_blank)
    img_tags = fields.Str(required=True, validate=must_not_be_blank)
    upload_date = fields.Str(required=True, validate=must_not_be_blank)


    def process_record(self, data, **kwargs):
        station_name1 = data.get("station")
        if station_name1:
            name, image = station_name1.split(" ")
            station_dict = dict(name=name, image=image)
        else:
            station_dict = {}
        data["station"] = station_dict
        return data
        #return station_dict

station_schema = StationSchema()
stations_schema = StationSchema(many=True)
record_schema = RecordSchema()
records_schema = RecordSchema(many=True)



# GET all Stations 
@app.route("/channels/", methods=["GET"])
def get_stations():
    stations = Station.query.all()

    result = stations_schema.dump(stations)
    print(result)
    #return {"stations":result}
    return jsonify(result)
    

#GET all Channel CSV
@app.route("/channels/csv/", methods=["GET"])
def get_stations_csv():
    stations = Station.query.all()
    result = stations_schema.dump(stations)
    return send_csv(result,"test.csv",["station_image", "station_name", "id","station_image","station_name","id"])


#GET all recordings CSV
@app.route("/recordings/csv/", methods=["GET"])
def get_recording_csv():
    records = Record.query.all()
    result = records_schema.dump(records, many=True)

    data = pd.json_normalize(result)
    text = (data.to_csv())

    f = open("recording.csv", "w")
    f.write(text)
    f.close()
    return send_file('recording.csv', mimetype='application/x-csv', attachment_filename='recording.csv', as_attachment=True)


#GET by single station 
@app.route("/channels/<int:id>")
def get_station(id):
    try:
        station = Station.query.get(id)
    except IntegrityError:
        return {"message": "Station not found."}, 400
    station_result = station_schema.dump(station)
    records_result = records_schema.dump(station.records.all())
    print(records_result)
    return jsonify(station_result)

#GET all records
@app.route("/recordings/", methods=["GET"])
def get_records():
    records = Record.query.all()
    result = records_schema.dump(records, many=True)
    return jsonify(result)


#GET requested record
@app.route("/recordings/<id>", methods=["GET"])
def get_record(id):
    try:
        record = Record.query.get(id)
    except IntegrityError:
        return {"message": "Record not found."}, 400
    result = record_schema.dump(record)
    return jsonify(result)
   

#POST new record
@app.route("/recordings/", methods=["POST"])
def new_record():
    json_data = request.get_json()
    if not json_data:
        return {"message": "no data json format"}, 400
    #validate and deserialise input
    # try:
    #     data = record_schema.load(json_data)
    # except ValidationError as err:
    #     return err.message, 422
    station_name =request.json['station_name']
    audio_url = request.json['audio_url']
    img_tags = request.json['img_tags']
    tags = request.json['tags']
    upload_date = request.json['upload_date']

    station = Station.query.filter_by(station_name=station_name).first()
    record = Record(audio_url=audio_url, tags=tags, img_tags=img_tags, upload_date=upload_date, station=station)
    db.session.add(record)
    db.session.commit()


    records = Record.query.all()
    result = records_schema.dump(records, many=True)
    return jsonify(result) 

#POST new channel
@app.route("/channels/", methods=["POST"])
def new_channel():
    json_data = request.get_json()
    station_name = request.json['station_name']
    station_image = request.json['station_image']
    station = Station(station_image=station_image, station_name=station_name)
    db.session.add(station)
    db.session.commit()
    
    stations = Station.query.all()
    result = stations_schema.dump(stations, many=True)
    return jsonify(result)

#PUT new record
@app.route("/recordings/<int:id>", methods=["PUT"])
def updateRecord(id):
    
    try:
        record = Record.query.get(id)
    except IntegrityError:
        return {"message": "Record could not be found"}, 404

    station_name =request.json['station_name']
    audio_url = request.json['audio_url']
    img_tags = request.json['img_tags']
    tags = request.json['tags']
    upload_date = request.json['upload_date']
    

    record.audio_url =audio_url
    record.img_tags=img_tags
    record.tags=tags
    record.upload_date=upload_date
    db.session.commit()
    return record_schema.dump(record)



# ===========================check if difference in records ======================
def checkDiff(id, recordList):
    record = Record.query.get(id)
    result = record_schema.dump(record)
    if recordList[1] == result['tags'] and recordList[2] == result['audio_url'] and recordList[3]==result['img_tags'] and recordList[4]==result['upload_date'] and int(recordList[5])==result['id'] and recordList[6]==result['station']['station_name'] and int(recordList[7])==result['station']['id'] and recordList[8]== result['station']['station_image']:
        print("all ok!!!")
    else:
        print("not ok!?!?!?!?!?!?!")
        recordList.pop(0)
        key = ['tags', 'audio_url', 'img_tags', 'upload_date', 'id', 'station.station_name', 'station.id', 'station.station_image']
        # dictionary = dict(zip(key, recordList))
        # print(dictionary)
        updateRecordFromCSV(id, recordList)

def updateRecordFromCSV(id, list):
    
    try:
        record = Record.query.get(id)
        station = Station.query.get(list[6])
    except IntegrityError:
        return {"message": "Record could not be found"}, 404

    station_name = list[5]
    audio_url = list[1]
    img_tags = list[2]
    tags = list[0]
    upload_date = list[3]
    
    satation_image = list[7]
    
    station.station_name = station_name
    station.station_image = satation_image

    record.audio_url =audio_url
    record.img_tags=img_tags
    record.tags=tags
    record.upload_date=upload_date
    db.session.commit()


@app.route("/test/", methods=["GET"])
def test():
    csvFilePath = 'del.csv'
    jsonFilePath = 'some.json'

    records = Record.query.all()
    result = records_schema.dump(records, many=True)
    json_ = request.json
    new = pd.read_csv(csvFilePath)
        


    with open(csvFilePath, 'r', newline='') as read_obj:
        csv_reader = reader(read_obj)
        objList = list(csv_reader)
        for recordList in objList:
            id = recordList[5]
            if id != "id":
                checkDiff(id, recordList)
                record = Record.query.get(id)
                result = record_schema.dump(record)

             


    something = get_records()
    return something

if __name__ == "__main__":
    db.create_all()
    app.run(host='0.0.0.0',debug=True, port=5000)

