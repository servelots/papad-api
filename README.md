#Papad
================
An application designed to listen and tag audio files by a CR station community. These tags (the meta information) can then be used to search, sort and share a large collection of audio files – eg., when community calls a CR station and leaves messages in large numbers, or when a CR Station wants to put their program content out so that the community can listen to it and also categorize it for their own convenience. To quote in one line, the tool is a community-oriented content curation for audio files. 

#About Papad API
-----------------
Papad API is API for Papad web-application written on Flask. Papad API recieves any audio file uploaded through the web application along with text tags and image tags. Images are stored in Raspberry Pi and 

#Requirements
----------------
Flask

#Hardware
----------------
Raspberry Pi 4

#Database
----------------
SQLite

#Source
----------------
https://gitlab.com/servelots/papad

#Files List:
-----------
PapadAPI  
├── app.py  (Main application file with routing, database model and other information)  
├── papad.db  (Database file with all the uploaded text data)  
├── README.md  (General Information)  
└── requirements.txt    (list of all the software requirements)  


#set up environment:
-----------------------
$ python3 -m venv papadenv

$ source papadenv/bin/activate

$ apt install python3-pip

$ pip install -r requirements.txt

#Close the environment:
----------------------
$ deactivate 


#Run application:
------------------
$ source papadenv/bin/activate

$ python app.py


application runs at http://0.0.0.0:/5000
    
#VIEW:
============

All Channels:
    method : GET
    URL    : http://51.158.167.121:5000/channels/
    Output : [

      {

        "id": 1,

        "station_image": "https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Solid_blue.svg/1024px-Solid_blue.svg.png",

        "station_name": "anthill"

      },

      {

        "id": 2,

        "station_image": "https://iruway.janastu.org/assets/icons/logo.png",

        "station_name": "iruway"

      }

    ]


Particular Channel:
    method : GET
    URL    : http://51.158.167.121:5000/channels/<id>
    Output : 
        {

      "id": 1,

      "station_image": "https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Solid_blue.svg/1024px-Solid_blue.svg.png",

      "station_name": "anthill"

    }

All Recordings:
    method : GET
    URL    : http://51.158.167.121:5000/recordings/
    Output :[

      {

        "audio_url": "https://www.examplesite.exampleAudio1.mp3",

        "id": 1,

        "img_tags": "https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Solid_blue.svg/1024px-Solid_blue.svg.png",

        "station": {

        "id": 1,

        "station_image": "https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Solid_blue.svg/1024px-Solid_blue.svg.png",

        "station_name": "anthill"

        },

        "tags": "tag2,tag1,tag3",

        "upload_date": "2020.12.31"

      },

      {

        "audio_url": "https://www.examplesite.exampleAudio2.mp3",

        "id": 2,

        "img_tags": "https://iruway.janastu.org/assets/images/img3.jpeg",

        "station": {

        "id": 1,

        "station_image": "https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Solid_blue.svg/1024px-Solid_blue.svg.png",

        "station_name": "anthill"

        },

        "tags": "tag33,tag44",

        "upload_date": "2020.12.2"

      },

    ]


Purticular Recording:
    method : GET
    URL    : http://51.158.167.121:5000/recordings/<id>
    Output : 
        {

      "audio_url": "https://www.examplesite.exampleAudio1.mp3",

      "id": 1,

      "img_tags": "https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Solid_blue.svg/1024px-Solid_blue.svg.png",

      "station": {

        "id": 1,

        "station_image": "https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Solid_blue.svg/1024px-Solid_blue.svg.png",

        "station_name": "anthill"

      },

      "tags": "tag2,tag1,tag3",

      "upload_date": "2020.12.31"

}
        
        
    
        
        
ENTER NEW DATA:
==============
All Recordings:
    method : POST
    URL    : http://51.158.167.121:5000/recordings/
    Input  : 
        {

        "audio_url": "https://www.examplesite.exampleAudio3.mp3",

        "img_tags": "https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Solid_blue.svg/1024px-Solid_blue.svg.png",

        "station_name": "anthill",

        "tags": "tag2,tag1,tagtag",

        "upload_date": "2020.12.31"

      }









EDIT EXISTING DATA:
===================
All Recordings:
    method : PUT
    URL    : http://51.158.167.121:5000/recordings/<id>
    Input  : 
        {

        "audio_url": "https://www.examplesite.exampleAudio2.mp3",

        "img_tags": "https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Solid_blue.svg/1024px-Solid_blue.svg.png",

        "station_name": "anthill",

        "tags": "tag2,tag1,tagtag",

        "upload_date": "2020.12.31"

      }


GET CSV OF ALL RECORDINGS:
===================
All Recordings:
    method : GET
    URL    : http://51.158.167.121:5000/recordings/csv
   

GET CSV OF ALL CHANNELS:
===================
All Recordings:
    method : GET
    URL    : http://51.158.167.121:5000/channels/csv
   