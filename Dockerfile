#FROM python:3.7-alpine
FROM tiangolo/meinheld-gunicorn-flask:python3.7
COPY . /app
WORKDIR /app
# RUN pip install -r ./requirements.txt
# RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories
# RUN apk add --no-cache --allow-untrusted --repository http://dl-3.alpinelinux.org/alpine/edge/testing hdf5 hdf5-dev
# RUN apk --no-cache --update-cache add gcc gfortran python python-dev py-pip build-base wget freetype-dev libpng-dev openblas-dev
# RUN ln -s /usr/include/locale.h /usr/include/xlocale.h
# RUN pip install --no-cache-dir numpy pandas
RUN . papad/bin/activate
COPY ./requirements.txt /requirements.txt
RUN pip install -r /requirements.txt
RUN pip install numpy pandas
#ENTRYPOINT ["python"]
#CMD ["app.py"]
